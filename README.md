# Little Docker project

The idea of this project was to try a little bit of Docker.

## Unique use of Docker
In the article above is described unique use of docker.
[unigue use](https://www.upguard.com/blog/top-11-weird-and-wonderful-uses-for-docker)

##Example of usage on gitlab
Above example of usage of docker on gitlab.
[usage on gitlab](https://gitlab.com/gitlab-examples/docker)

##Usage that is not said in class
Use of docker in embedded software.
[ebmo docker](https://dev.to/dalimay28/using-docker-for-embedded-systems-development-b16)
## Getting Started

Take a git clone on project and take it to your computer. 
### Prerequisites

You need a windows or linux computer, in which you can build the project, Docker to run docker.

# About the project

This was made by Ilona Skarp for Noroff and Experis Academy Finland.
